<?php

namespace App\Http\Controllers;

use App\Models\User;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\VideoRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function profile(){
        return view('auth.image',array('user' => Auth::user()));
    }

    public function update_profile(ImageRequest $request){
        if($request->hasFile('avatar')){
            $filename = $request->avatar->getClientOriginalName();
            $request->avatar->storeAs('public/avatars',$filename);  
            $user = Auth::user();
            User::where('id', $user->id)
                    ->where('status', 1)
                    ->update(['avatar' => $filename]);
            // return view('auth.image')->with('user',$user);
        }
        return redirect()->route('update_profile')->with('success','Image Updated Successfully!!');
    }

    public function video(){
        $user = Auth::user();
        return view('auth.video')->with('user', $user);
    }
    public function uploadvideo(VideoRequest $request){
        if($request->hasFile('video')){
            $filename = $request->video->getClientOriginalName();
            $request->video->storeAs('public/videos',$filename);
            $user = Auth::user();
            User::where('id',$user->id)
                ->where('status',1)
                ->update(['video'=>$filename]);
            //return view('auth.video')->with('user',$user);
        }
        return redirect()->route('uploadvideo')->with('success','Video Updated Successfully!!');
    }
}
