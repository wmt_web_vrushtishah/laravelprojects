<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Friendship;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

class UserController extends Controller
{
    public function index(){
        $id=auth()->user()->id;
        $user = User::with('friends')->where('id',$id)->get();
        return view('users.profile',compact('user'));
    }
}
