<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Redirect;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'required|image|mimes:png,jpeg,jpg|max:2048',
        ];

    }


    protected function failedValidation(Validator $validator)
    {
        if($validator->fails()){
            $error = $validator->errors()->first();
        }
        throw new HttpResponseException(
            Redirect::back()->withErrors($error)
        );
    }
}
