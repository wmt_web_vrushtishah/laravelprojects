<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    use HasFactory;
    
    protected $table = 'friends_users';

    protected $primaryKey = 'id';

    protected $fillable = ['friend_name', 'friend_id','user_id','status'];
}

