<?php

namespace Database\Seeders;
use App\Models\Friendship;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class FriendshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<25;$i++){
            $friendship = [
                'friend_name' => Str::random(6),
                'friend_id' => mt_rand(1,13),
                'user_id' => mt_rand(1,13),
            ];
            Friendship::insert($friendship);
        }
    }
}
