@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif </br>

            <img src="storage/avatars/{{ @$user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
           <h2>{{ $user->name }}'s Profile</h2>
           <form enctype="multipart/form-data" action="{{route('profile')}}" method="POST">
                <label>Update Profile Image</label></br>
                    <input type="file" name="avatar"></br>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></br>
                    <input type="submit" class="pull-right btn btn-sm btn-success">
           </form>
        </div>
    </div>
</div>
@endsection
