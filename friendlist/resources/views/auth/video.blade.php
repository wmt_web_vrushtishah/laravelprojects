@extends('layouts.app')

@section('content')
<div class="container">
    <div class="content">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                 </ul>
             </div>
        @endif
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif </br>
        <h2>{{ $user->name }}'s Video</h2>
       <video width="320" height="240" controls>
            <source src="storage/videos/{{ @$user->video}}" type="video/mp4">
       </video>
        <h1>Video Upload</h1>
        <form action="{{ route('video') }}" method="POST" enctype="multipart/form-data">
            <label>Select video to upload</label>
            <input type="file" name="video" id="video">
            <input type="submit" value="Upload" name="submit" class="btn btn-primary">
            <input type="hidden" value="{{ csrf_token() }}" name="_token">
        </form>
    </div>
</div>

@endsection