@extends('layouts.app')
@section('content')
<div class="container">
    <div class="top">
        <h1>Friends List</h1>
    </div>
    @foreach($user as $user)
        <div>
            <span>Name: {{ $user->name }}</span></br>
            <span>Email: {{ $user->email }}</span></br>
                @forelse($user->friends as $friend)
                    <ul>
                        <li>{{$friend->friend_id}} : {{$friend->friend_name}}</li>
                    </ul>
                    @empty
                        <strong class="text text-danger">no friends to show</strong>
                @endforelse
        </div>
    @endforeach
    <a href="{{route('video')}}">Click to upload video</a>
</div>
@endsection