<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/profile',[App\Http\Controllers\HomeController::class,'update_profile'])->name('update_profile');
Route::get('/profile',[App\Http\Controllers\HomeController::class,'profile'])->name('profile');
Route::post('/uploads',[App\Http\Controllers\HomeController::class,'uploadvideo'])->name('uploadvideo');
Route::get('/uploads',[App\Http\Controllers\HomeController::class,'video'])->name('video');
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/friends',[App\Http\Controllers\UserController::class,'index'])->name('index');