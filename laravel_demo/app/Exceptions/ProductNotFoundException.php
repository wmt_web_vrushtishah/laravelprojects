<?php

namespace App\Exceptions;

use Exception;

class ProductNotFoundException extends Exception
{
    public function report(){
        
    }

    public function render(){
        return view('errors.notfound');
    }
}
