<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\failedValidation;
use App\Models\Product;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\ViewErrorBag;

class CreateProductRequest extends FormRequest implements ValidatesWhenResolved
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|max:20',
            'description' => 'required|string',
            'price' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return[
            'product_name.required' => 'Product is must',
            'description.required' => 'Description is required',
            'price.required' => 'Price is requied',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if($validator->fails()){
            $error = $validator->errors()->first();
        }
        throw new HttpResponseException(
            Redirect::back()->withErrors($error)
        );
        throw (new ValidationException($validator))->errorBag($error)->redirectTo($this->getRedirectUrl());
    }
}
