<?php

namespace App\Services;
use App\Models\Product;
use Illuminate\Support\ServiceProvider;
use App\Exceptions\ProductNotFoundException;

class ProductService{
    public function findByid($product){
        $product = Product::where($product)->first();
        if(!$product){
            throw new ProductNotFoundException('Product not found'.$product);
        }
        return $product;
    }
}

?>