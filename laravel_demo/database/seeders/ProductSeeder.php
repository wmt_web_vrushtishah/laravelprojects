<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Str;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       for($i=0;$i<5;$i++){
           $products=[
               'product_name' => Str::random(5),
               'description' => Str::random(5),
               'price' => mt_rand(10,100),
           ];
           Product::insert($products);
       }
    }
}
