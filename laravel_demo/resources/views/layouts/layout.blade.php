<main class="px-3">
    <h1>Product Application</h1>
    <p class="lead">By clicking below link provided you will get to know what products are available with us..</p>
    <p class="lead">
      <a href="{{route('product.list')}}" class="btn btn-lg btn-secondary fw-bold border-white bg-white">Product List</a>
    </p>
  </main>