@extends('product.layout')
@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Check all Products</h2>
            </div>
            <div class="pull-left">
                <a class="btn btn-success mb-sm-4" href="{{ route('products.create') }}"> Create new Product</a>
            </div>
        </div> 
</div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <span>{{ $message }}</span>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->product_name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->price }}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                    <a class="btn btn-info btn-sm" href="{{ route('products.show',$product->id) }}">Show</a> |
                    <a class="btn btn-primary btn-sm" href="{{ route('products.edit',$product->id) }}">Edit</a> |
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm del" onClick="confirmDelete()">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection

@section('scripts')
<script>
    function confirmDelete(){
        //console.log('test123')
        if(confirm("Delete Record?")==true){
            //alert('Now deleting')
            return true;
        }
    }
</script>
@endsection
