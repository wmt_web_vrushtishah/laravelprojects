<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Main Layout</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> 
</head>
<body>
    <div class="container">
        @yield('content')
        @yield('scripts')
    </div>
    <div class="jumbotron text-center" style="margin-bottom:0">
        <span>Footer</span>
    </div>
</body>
</html>