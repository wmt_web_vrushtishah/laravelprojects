<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Jobs\SendEmailJob;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){
        return view('home');
    }

    // public function store(Request $request){
    //     $details = [
    //         'name' => $request->name,
    //         'email' => $request->email,
    //          ];
    //     dispatch(new SendEmailJob($details));
    //         return redirect()->route('email.testemail');
    // }

    // public function create(array $details){
    //     $user = User::create([
    //         'name' => $details['name'],
    //         'email' => $details['email'],
    //     ]);
    //     Mail::to($user->email)->send(new SendEmailJob($user));
    // }
}
