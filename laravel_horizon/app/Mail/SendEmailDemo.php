<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendEmailDemo extends Mailable
{
    use Queueable, SerializesModels;
    protected $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details=$details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Log::info($this->details);
        // return $this->subject('Recipe Submission - ')
        //           ->from('contact@webmob.tech', 'Primally Nourished')
        //           ->view('email.testemail')
        //           ->with('details', $this->details);
        return $this->view('email.testemail')->with("details",$this->details);
    }
}
