<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\failedValidation;
use App\Models\Email;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\ViewErrorBag;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;   
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'subject' => 'required|max:150',
            'body' => 'required',
        ];
    }

    public function messages()
    {
        return[
            'title.required' => 'Title is must',
            'subject.required' => 'Subject is required to know about topic',
            'body.required' => 'Description of title is required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if($validator->fails()){
            $error = $validator->errors()->first();
        }
        throw new HttpResponseException(
            Redirect::back()->withErrors($error)
        );
    }
}
