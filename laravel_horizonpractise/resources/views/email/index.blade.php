<!DOCTYPE html>
<html style="height: 100%;" lang="en">
    <head>
    <meta name="viewport" content="width=device-width, minimum-scale=0.1">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>

    <body style="margin: 0px; height: 100%; background-image:url('https://images.pexels.com/photos/132197/pexels-photo-132197.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940')">
        <div class="container" id="container">
            <div class="row">
                <div class="justify-content-center col-md-4 p-4">
                    <h2>Send Email</h2>
                    @if ($errors->any())
                        <div class="alert alert-danger row-cols-1">
                            <strong>Warning!!</strong><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <span> {{ $message }}</span>
                        </div>
                    @endif  
                    <div class="row justify-content-center">
                        <form action="{{route('email.store')}}" method="post">
                            <div class="justify-content-center form-group d-flex row">
                            @csrf
                                <div class="form-group mb-2">
                                    <label for="title" class="col-sm-2 col-form-label">Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" placeholder="Title" class="form-control form-control-lg"/>
                                        </div>
                                </div>
                                <div class="form-group mb-2">
                                    <label for="subject" class="col-sm-2 col-form-label">Subject</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="subject" placeholder="Subject" class="form-control form-control-lg"/>
                                        </div>
                                </div>
                                <div class="form-group mb-2">
                                    <label for="body" class="col-sm-2 col-form-label">Body</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" name="body" placeholder="Body" class="form-control form-control-lg"></textarea>
                                        </div>
                                </div>
                                <div class="form-group mb-2">
                                    <div class="col-sm-10">
                                        <input type="submit" name="submit" value="Send Email" class="btn btn-success"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
