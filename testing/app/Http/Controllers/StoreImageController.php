<?php

namespace App\Http\Controllers;
use App\Models\Images;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\ImageRequest;

class StoreImageController extends Controller
{
    public function index(){
        $data = Images::latest()->paginate(5);
        return view('store_image',compact('data'))->with('i',(request()->input('page',1)-1)*5);
    }

    public function insert_image(ImageRequest $request){
        $image = new Images($request->input());
        if($file = $request->hasFile('user_image')){
            $file = $request->file('user_image');
            $filename = $file->getClientOriginalName();
            $destinationPath = public_path().'/images/';
            $file->move($destinationPath,$filename);
            $image->user_image = $filename;
        }
        $image->save();
        return redirect()->route('index')->with('success','Image stored successfully');
    }
}
