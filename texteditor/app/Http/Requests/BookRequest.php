<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Redirect;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:30',
            'author_name' => 'required|string|max:20',
            'description' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name of book is required.',
            'author_name.required' => 'Author name is must.',
            'description.required' => 'Description of the book is required.'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if($validator->fails()){
            $error = $validator->errors()->first();
        }
        throw new HttpResponseException(
            Redirect::back()->withErrors($error)
        );
    }
}
