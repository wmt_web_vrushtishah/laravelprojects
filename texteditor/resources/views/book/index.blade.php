<!DOCTYPE html>
<html>
<head>
<head>
    <title>Text Editor</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha256-aAr2Zpq8MZ+YA/D6JtRD3xtrwpEz2IqOS+pWD/7XKIw=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha256-OFRAJNoaD8L3Br5lglV7VyLRf0itmoBzWUoM+Sji4/8=" crossorigin="anonymous"></script>
</head>
<body>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Check All Books List</h2>
        </div>
        <div class="pull-left">
            <a class="btn btn-primary mb-sm-2" href="{{route('books.create')}}">Add New Book</a>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <span>{{ $message }}</span>
    </div> 
@endif
<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Book Name</th>
        <th>Author Name</th>
        <th>Description</th>
        <th width="250px">Action</th>
    </tr>
    @foreach($books ?? '' as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td>{{ $book->name }}</td>
            <td>{{ $book->author_name }}</td>
            <td>{{ $book->description }}</td>
            <td>
                <form action="{{ route('books.destroy',$book->id) }}" method="POST">
                    <a class="btn btn-info btn-sm" href="{{route('books.show',$book->id)}}">Show Details</a> | 
                    <a class="btn btn-info btn-sm" href="{{route('books.edit',$book->id)}}">Edit</a> |
                    @csrf
                    @method ('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm del" onClick="confirmDelete()">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>

    <script>
        function confirmDelete(){
            if(confirm("Delete Record??")==true){
                return true;
            }
        }
    </script>
</body>
</html>